$(document).ready(function () {
    $('.link-more').append('<svg class="arrow-icon" xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32"> <g fill="none" stroke="#064CBE" stroke-width="1.5" stroke-linejoin="round" stroke-miterlimit="10"> <circle class="arrow-icon--circle" cx="16" cy="16" r="15.12"></circle> <path class="arrow-icon--arrow" d="M16.14 9.93L22.21 16l-6.07 6.07M8.23 16h13.98"></path> </g> </svg>');
    $('.btn-menu').on('click', function () {
        $(this).toggleClass('open');
        $('.header-nav').toggleClass('open').fadeToggle();
        $('body').toggleClass('menu-open');
    });

    $('.scroll-page').on('click', function (e) {
        e.preventDefault();
        var body = $('html, body');
        var $target = $($(this).attr('href'));
        if ($target.length) {
            $('html, body').animate({
                'scrollTop': $target.offset().top - 60
            }, 500);
        }
        if ($('.btn-menu').hasClass('open')) $('.btn-menu').removeClass('open');
        if ($('.header-nav').hasClass('open')) $('.header-nav').removeClass('open').fadeOut();
        if ($('body').hasClass('menu-open')) $('body').removeClass('menu-open');

        return false;
    });
    $(document).on('click', '[data-dismiss^=modal]', function() {
        if ($('body').hasClass('menu-open'))  $('body').removeClass('menu-open');
        if ($('.header-menu').hasClass('open'))  $('.header-menu').removeClass('open');
    });

    $(document).on('click', '.close', function() {
        $(".modal-video iframe").each(function () {
            $(this)[0].contentWindow.postMessage('{"event":"command","func":"pauseVideo","args":""}', '*')
        });
    });

    /*$(document).on('click', '.section-about-team_item', function() {
        if (!$(this).hasClass('open')){
            $('.section-about-team_item').removeClass('open');
           $(this).addClass('open');
        }
        else{
            $(this).removeClass('open');
        }
    });*/

    $( ".form-control" ).change(function() {
        $(this).parent().addClass('focus');
    });

    var inputs = document.querySelectorAll( '.inputfile' );
    Array.prototype.forEach.call( inputs, function( input )
    {
        var label	 = input.nextElementSibling,
            labelVal = label.innerHTML;

        input.addEventListener( 'change', function( e )
        {
            var fileName = '';
            if( this.files && this.files.length > 1 )
                fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
            else
                fileName = e.target.value.split( '\\' ).pop();

            if( fileName )
                label.querySelector( 'span' ).innerHTML = "<span class='selected'>"+fileName+"</span>";
            else
                label.innerHTML = labelVal;
        });

        // Firefox bug fix
        input.addEventListener( 'focus', function(){ input.classList.add( 'has-focus' ); });
        input.addEventListener( 'blur', function(){ input.classList.remove( 'has-focus' ); });
    });

    $(window).on('scroll', function(){
        var scroll = $(window).scrollTop();
        if ($("div").is(".page-career")){
            var block = $('.page-career').offset().top;
            if(scroll +110 >= block ){
                $('.page-career').addClass('fix');
            } else {
                $('.page-career').removeClass('fix');
            }
        }

    });
});
